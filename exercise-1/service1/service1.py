import logging
import os
import socket
import time
from datetime import datetime, timezone

import requests


def post_request(address, data):
    response = requests.post(address, json=data)
    if response.status_code != 200:
        raise Exception("non-OK response code received")

    return response


if __name__ == '__main__':

    logging.basicConfig(level=logging.INFO, format='%(message)s',
                        handlers=[logging.FileHandler('/var/log/service1/service1.log', mode='w'),
                                  logging.StreamHandler()])

    host = os.environ.get('host', 'localhost')
    port = os.environ.get('hort', '8000')

    try:
        host = socket.gethostbyname(host)
    except Exception as e:
        logging.getLogger().error(e)

    if host[:7] != "http://":
        host = "http://" + host

    address = f"{host}:{port}/process"

    for i in range(1, 21):
        try:
            message = f"{i} {datetime.now(tz=timezone.utc)} {address}"
            _ = post_request(address, {'Message': message})
            logging.info(message)
        except Exception as e:
            logging.error(e)
        time.sleep(2)

    try:
        _ = post_request(address, {'Message': "STOP"})
        logging.getLogger().info("STOP")
    except Exception as e:
        logging.error(e)
