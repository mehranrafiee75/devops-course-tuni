package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"io"
	"log"
	"mehran.com/server/internal/models"
	"net/http"
	"os"
	"time"
)

var c chan bool

func main() {

	logfile, err := os.OpenFile("/var/log/service2/service2.log", os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		panic(err)
	}

	defer logfile.Close()
	log.SetOutput(logfile)
	log.SetFlags(0) // disable all other fields. timestamp, Level, etc.

	time.Sleep(2 * time.Second)
	port, ok := os.LookupEnv("port")
	if !ok {
		port = "8000"
	}

	c = make(chan bool)
	router := mux.NewRouter()

	router.HandleFunc("/process", processCall).Methods(http.MethodPost, http.MethodOptions)

	server := http.Server{
		Addr:         ":" + port,
		Handler:      router,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	go func() {
		<-c
		_ = server.Close()
		_ = logfile.Close()
	}()

	_ = server.ListenAndServe()

}

func processCall(w http.ResponseWriter, r *http.Request) {

	data, err := io.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
		writeResponse(w, models.Response{Message: "failed to read the request body"}, http.StatusInternalServerError)
		return
	}

	var requestData models.Request
	err = json.Unmarshal(data, &requestData)
	if err != nil {
		log.Println(err)
		writeResponse(w, models.Response{Message: "bad request"}, http.StatusBadRequest)
		return
	}

	log.Printf("%s %s", requestData.Message, r.RemoteAddr)
	if requestData.Message == "STOP" {
		writeResponse(w, models.Response{Message: "service2 is shutting down!"}, http.StatusOK)
		c <- true
		return
	} else {
		writeResponse(w, models.Response{Message: fmt.Sprintf("%s %s", requestData.Message, r.RemoteAddr)}, http.StatusOK)
	}
}

func writeResponse(w http.ResponseWriter, response models.Response, status int) {

	data, err := json.Marshal(response)
	if err != nil {
		log.Println(err)
		w.WriteHeader(500)
		return
	}

	w.WriteHeader(status)
	_, err = w.Write(data)
	if err != nil {
		log.Println(err)
	}
}
